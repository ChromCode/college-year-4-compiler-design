// $Id: main.cpp,v 1.2 2016-08-18 15:13:48-07 - - $

#include <assert.h>
#include <cstdlib>
#include <ctype.h>
#include <fstream>
#include <iostream>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <vector>
#include <unistd.h>

#include "auxlib.h"
#include "string_set.h"
#include "lyutils.h"

using namespace std;

constexpr size_t LINESIZE = 1024;

string tFile;
string CPP = "/usr/bin/cpp";

extern int yy_flex_debug;
extern int yydebug;

extern FILE* yyin;
extern FILE* outFile;

/*
 *  The functions chomp and cpplines were obtained and modified from
 *  /afs/cats.ucsc.edu/courses/cmps104a-wm/Assignments/code/cppstrtok
 *  Original Author: Wesley Mackey
*/

// Chomp the last character from a buffer if it is delim.
void chomp (char* string, char delim) {
   size_t len = strlen (string);
   if (len == 0) return;
   char* nlpos = string + len - 1;
   if (*nlpos == delim) *nlpos = '\0';
}

void cpplines (FILE* pipe, const char* filename) {
   int linenr = 1;
   char inputname[LINESIZE];
   strcpy (inputname, filename);
   for (;;) {
      char buffer[LINESIZE];
      char* fgets_rc = fgets (buffer, LINESIZE, pipe);
      if (fgets_rc == NULL) break;
      chomp (buffer, '\n');
      sscanf (buffer, "# %d \"%[^\"]\"",
            &linenr, inputname);

      // .str file
      const char* baseFile = (tFile.substr(0, tFile.size()-3) 
                              + ".tok").c_str();
      outFile = fopen(baseFile, "w");
      int yy_lc = yyparse();
      fclose(outFile);

      // .ast file
      baseFile = (tFile.substr(0, tFile.size()-3) + ".ast").c_str();
      outFile = fopen(baseFile, "w");
      astree::print(outFile, parser::root);
      fclose(outFile);
      ++linenr;

      if(yy_lc != 0) { 
         cout << "ERROR in yy_lc" << endl;
         exit (EXIT_FAILURE);
      }
   }
}

int main (int argc, char** argv) {
   string command;
   int opt;

   yy_flex_debug = 0;
   yydebug = 0;

   while ((opt = getopt(argc, argv, "@:D:ly")) != -1) {
      switch (opt) {
         case '@': set_debugflags(optarg);
                   break;
         case 'D': CPP = CPP + " -D " + optarg;
                   break;
         case 'l': yy_flex_debug = 1;
                   break;
         case 'y': yydebug = 1;
                   break;
      }
   }

   // check for file after options
   if (optind  > argc) {
      cerr << "Incorrect Usage og Program, please provide a .oc file"
           << endl;
      exit (EXIT_FAILURE);
   }

   tFile = basename (argv[optind]);

   // Check for appropiate ending
   size_t i = tFile.rfind('.', tFile.length());
   if( tFile.substr(i, tFile.length()-i) != ".oc"
                           || i == string::npos )
   {
      cerr << "Incorrect Usage of Program, please provide a .oc file"
           << endl;
      exit(EXIT_FAILURE);
   }

   exec::execname = basename (argv[0]);
   char* inFileName = argv[optind];
   command = CPP + " " + inFileName;
   
   DEBUGF('c', "command=\"%s\"\n", command.c_str());
   yyin = popen (command.c_str(), "r");
   if (yyin == NULL) {
      cerr << (command.c_str()) << " is invalid" << endl;
      exit (EXIT_FAILURE);
   } else {
      cpplines (yyin, inFileName);
      int pclose_rc = pclose (yyin);
      if (pclose_rc != 0)  {
         exit (EXIT_FAILURE);
      }
   }

   string file = tFile.substr(0, tFile.size()-3) + ".str";
   const char* fileName = file.c_str();
   FILE* outFile = fopen(fileName, "w");

   string_set::dump(outFile);
   fclose(outFile);

   return EXIT_SUCCESS;
}

