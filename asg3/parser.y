%{
#include <cassert>
#include <stdlib.h>
#include <string.h>

#include "astree.h"
#include "lyutils.h"

%}

%debug
%defines
%error-verbose
%token-table
%verbose

%destructor { destroy ($$); } <>
%printer { astree::dump (yyoutput, $$); } <>

%initial-action {
   parser::root = new astree (ROOT, {0, 0, 0}, "<<ROOT>>");
}

%token  ROOT TOK_IDENT NUMBER
%token  TOK_IF TOK_ELSE TOK_WHILE TOK_RET
%token  TOK_INT TOK_STRING TOK_STRUCT TOK_VOID
%token  TOK_NEW TOK_NULL TOK_ARRAY
%token  TOK_INT_CONST TOK_CHAR_CONST TOK_STR_CONST
%token  TOK_EQ TOK_NEQ TOK_LEQ TOK_GEQ

%token  TOK_ROOT TOK_DECLID TOK_TYPEID TOK_FIELD
%token  TOK_INDEX TOK_POS TOK_NEG TOK_CALL
%token  TOK_NEWARRAY TOK_NEWSTR TOK_BLOCK
%token  TOK_VARDECL TOK_FUNCTION TOK_PARAM TOK_PROTO
%token  UNOP BINOP TOK_EXC TOK_IFELSE TOK_RETURNVOID

%token  '('  ')'  '['  ']'  '{'  '}'  ';'  ','  '.'
%token  '='  '+'  '-'  '*'  '/'  '%'  '!'

%right TOK_IF TOK_ELSE
%right  '='

%left TOK_EQ TOK_NEQ '>' TOK_GEQ '<' TOK_LEQ
%left   '+' '-'
%left   '*' '/'
%right  '^'
%right  POS NEG TOK_EXC TOK_NEW
%left '[' '.'

%nonassoc '('

%start  start

%%
start     : program                   { $$ = $1 = nullptr; }
          ;

program   : program structdef         { $$ = $1->adopt ($2); }
          | program function          { $$ = $1->adopt ($2); }
          | program stmt              { $$ = $1->adopt ($2); }
          | program error '}'         { destroy ($3); $$ = $1; }
          | program error ';'         { destroy ($3); $$ = $1; }
          |                           { $$ = parser::root; }
          ;

structdef : TOK_STRUCT TOK_IDENT '{' '}'  { destroy ($3, $4);
                                        $2->sym_swap (TOK_TYPEID);
                                        $$ = $1->adopt ($2); }
          | TOK_STRUCT TOK_IDENT '{' fields '}' 
                                      { destroy ($3, $5);
                                        $2->sym_swap (TOK_TYPEID);
                                        $$ = $1->adopt ($2, $4); }

field     : basetype TOK_IDENT            { $2->sym_swap (TOK_FIELD);
                                         $$ = $1->adopt ($2); }
          | basetype TOK_ARRAY TOK_IDENT  { $2->sym_swap (TOK_ARRAY);
                                        $3->sym_swap (TOK_FIELD);
                                        $$ = $2->adopt ($1, $3); }
          ;

basetype  : TOK_VOID                  { $$ = $1; }
          | TOK_INT                   { $$ = $1; }
          | TOK_STRING                { $$ = $1; }
          | TOK_IDENT                     { $1->sym_swap (TOK_TYPEID); 
                                        $$ = $1; }
          ;

fields    : field ';' fields                
                                      { destroy ($2);
                                        $$ = $1->adopt ($3); }
          | field ';'                  { destroy ($2); $$ = $1; }
          ;

function  : TOK_IDENTdecl '(' TOK_IDENTdecls ')' block 
                                      { $4->sym_swap (TOK_FUNCTION);
                                        $2->sym_swap (TOK_PARAM);
                                        $2->adopt ($3);
                                        $$ = $4->adopt ($1, $2, $5); }
          |  TOK_IDENTdecl '(' TOK_IDENTdecls ')' ';' 
                                      { destroy($5);
                                        $4->sym_swap (TOK_FUNCTION);
                                        $2->sym_swap (TOK_PARAM);
                                        $2->adopt ($3);
                                        $$ = $4->adopt ($1, $2); }
          | TOK_IDENTdecl '(' ')' ';'     { destroy ($4);
                                        $3->sym_swap (TOK_PROTO);
                                        $2->sym_swap (TOK_PARAM);
                                        $$ = $3->adopt ($1, $2); }
          | TOK_IDENTdecl '(' ')' block   { $3->sym_swap(TOK_FUNCTION);
                                        $2->sym_swap(TOK_PARAM);
                                        $$ = $3->adopt ($1, $2, $4); }
          ;

TOK_IDENTdecl : basetype TOK_ARRAY TOK_IDENT  { $3->sym_swap (TOK_DECLID);
                                        $$ = $1->adopt ($2, $3); }
          | basetype TOK_IDENT            { $2->sym_swap (TOK_DECLID);
                                        $$ = $1->adopt ($2); }
          ;

TOK_IDENTdecls: TOK_IDENTdecls ',' TOK_IDENTdecl  { destroy ($2);
                                        $$ = $1->adopt ($3); }
          |  TOK_IDENTdecl                { $$ = $1; }
          ;

stmt      : block                     { $$ = $1; }
          | expr ';'                  { destroy ($2); $$ = $1; }
          | ifelse                    { $$ = $1; }
          | return                    { $$ = $1; }
          | vardecl                   { $$ = $1; }
          | while                     { $$ = $1; }
          | ';'                       { $$ = $1; }
          ;

stmts     : stmts stmt                { $$ = $1->adopt ($2); }
          | '{' stmt                  { $$ = $1->adopt ($2); }
          ;

block     : stmts '}'                 { destroy ($2);
                                        $1->sym_swap (TOK_BLOCK);
                                        $$ = $1; }
          | '{' '}'                   { destroy ($2);
                                        $1->sym_swap (TOK_BLOCK);
                                        $$ = $1; }
          ;

expr      : alloc                     { $$ = $1; }
          | binop                     { $$ = $1; }
          | call                      { $$ = $1; }
          | constant                  { $$ = $1; }
          | unop                      { $$ = $1; }
          | variable                  { $$ = $1; }
          | '(' expr ')'              { destroy ($1, $3); $$ = $2; }
          ;

alloc     : TOK_NEW TOK_IDENT '(' ')'        
                                      { destroy ($3, $4);
                                        $2->sym_swap (TOK_TYPEID);
                                        $$ = $1->adopt ($2); }
          | TOK_NEW TOK_STRING '(' expr ')' 
                                      { destroy ($2, $3, $5);
                                        $1->sym_swap (TOK_NEWSTR);
                                        $$ = $1->adopt ($4); }
          | TOK_NEW basetype '[' expr ']' 
                                      { destroy ($3, $5);
                                        $1->sym_swap (TOK_NEWARRAY);
                                        $$ = $1->adopt ($2, $4); }

binop     : expr '=' expr             { $$ = $2->adopt ($1, $3); }
          | expr '+' expr             { $$ = $2->adopt ($1, $3); }
          | expr '-' expr             { $$ = $2->adopt ($1, $3); }
          | expr '*' expr             { $$ = $2->adopt ($1, $3); }
          | expr '/' expr             { $$ = $2->adopt ($1, $3); }
          | expr '>' expr             { $$ = $2->adopt ($1, $3); }
          | expr '<' expr             { $$ = $2->adopt ($1, $3); }
          | expr '^' expr             { $$ = $2->adopt ($1, $3); }
          | expr TOK_LEQ expr         { $$ = $2->adopt ($1, $3); }
          | expr TOK_GEQ expr         { $$ = $2->adopt ($1, $3); }
          | expr TOK_EQ expr          { $$ = $2->adopt ($1, $3); }
          | expr TOK_NEQ expr         { $$ = $2->adopt ($1, $3); }

call      : params ')'                { destroy ($2);
                                        $1->sym_swap (TOK_CALL);
                                        $$ = $1; }
          | TOK_IDENT '(' ')'             
                                      { destroy ($3);
                                        $2->sym_swap (TOK_CALL);
                                        $$ = $2->adopt ($1); }
          ;

constant  : TOK_STR_CONST             { $$ = $1; }
          | TOK_CHAR_CONST            { $$ = $1; }
          | TOK_INT_CONST             { $$ = $1; }
          | TOK_NULL                  { $$ = $1; }
          ;

unop      : '+' expr %prec POS     
                                      { $$ = $1->adopt_sym 
                                       ($2, TOK_POS); }
          | '-' expr %prec NEG        { $$ = $1->adopt_sym 
                                       ($2, TOK_NEG); }
          | '!' expr %prec TOK_EXC    { $$ = $1->adopt_sym 
                                       ($2, TOK_EXC); }

variable  : TOK_IDENT                     { $$ = $1; }
          | expr '[' expr ']'         { destroy ($4);
                                        $2->sym_swap (TOK_INDEX);
                                        $$ = $2->adopt ($1, $3); }
          | expr '.' TOK_IDENT            { $3->sym_swap(TOK_FIELD);
                                        $$ = $2->adopt ($1, $3); }
          ;

ifelse    : TOK_IF '(' expr ')' stmt %prec TOK_IF 
                                      { destroy ($2, $4);
                                        $$ =$1->adopt($3,$5); }
          | TOK_IF '(' expr ')' stmt TOK_ELSE stmt
                                      { destroy ($2, $4);
                                        destroy ($5, $6);
                                        $1->sym_swap (TOK_IFELSE);
                                        $$ = $1->adopt ($3, $5, $7); }

return    : TOK_RET ';'               { destroy ($2);
                                        $1->sym_swap (TOK_RETURNVOID);
                                        $$ = $1; }
          | TOK_RET expr ';'          { destroy ($3);
                                        $$ = $1->adopt ($2); }
          ;

vardecl   : TOK_IDENTdecl '=' expr ';'    { destroy ($4);
                                        $2->sym_swap (TOK_VARDECL);
                                        $$ = $2->adopt ($1, $3); }
          ;

while     : TOK_WHILE '(' expr ')' stmt        
                                      { destroy ($2, $4);
                                        $$ = $1->adopt($3, $5); }
          ;

params    : TOK_IDENT '(' expr            { $$ = $2->adopt ($1, $3); }
          |  params ',' expr          { destroy ($2);
                                        $$ = $1->adopt ($3); }

%%

const char* parser::get_tname (int symbol) {
   return yytname [YYTRANSLATE (symbol)];
}

bool is_defined_token (int symbol) {
   return YYTRANSLATE (symbol) > YYUNDEFTOK;
}

/*
static void* yycalloc (size_t size) {
   void* result = calloc (1, size);
   assert (result != nullptr);
   return result;
}
*/

