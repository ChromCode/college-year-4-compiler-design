#ifndef __SYMTABLE_H__
#define __SYMTABLE_H__

#include <string>
#include <vector>
#include <bitset>
#include <unordered_map>
#include <iostream>
using namespace std;

#define GLOBAL 0

struct astree;
struct symbol;
struct location;

using namespace std;

enum { 
  ATTR_array, ATTR_bool, ATTR_char, ATTR_const, ATTR_field, 
  ATTR_function, ATTR_int, ATTR_lval, ATTR_null, ATTR_param, 
  ATTR_prototype, ATTR_string, ATTR_struct, ATTR_typeid,
  ATTR_vaddr, ATTR_variable, ATTR_void, ATTR_vreg,
  ATTR_bitset_size,
};

using attr_bitset = bitset<ATTR_bitset_size>;
using symbol_table = unordered_map<string*, symbol*>;
using symbol_entry = symbol_table::value_type;

#include "auxlib.h"
#include "lyutils.h"
#include "astree.h"

struct symbol {
   attr_bitset attributes;
   string struct_name;
   symbol_table* fields;
   vector<symbol*>* parameters;
   size_t block_nr;
   size_t filenr, linenr, offset;
};

symbol* get_symbol (astree* node);
symbol* set_function (astree* node);
symbol* set_symbol (astree* node);

void print_attributes (symbol* sym, string* name, FILE* out);
void set_attributes (astree* node, symbol* sym);
void setup_symtable();

bool check_return (symbol* sym1, symbol* sym2);
bool check_type (astree* node);
bool var_search_in_scope(string* name);
bool matches_with (astree* node1, astree* node2, 
	               string struct1, string struct2);
bool scan_table (astree* node, FILE* out);
bool var_search(string* name);

#endif

