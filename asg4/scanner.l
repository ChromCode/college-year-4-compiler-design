
%{

#include "lyutils.h"
#include "astree.h"

#define YY_USER_ACTION  { lexer::advance(); }

extern FILE* outFile;

#define RETURN(SYMBOL) { return yylval_token (SYMBOL); }

%}

%option 8bit
%option debug
%option nodefault
%option noinput
%option nounput
%option noyywrap
%option warn
/*%option verbose*/

LETTER          [A-Za-z_]
DIGIT           [0-9]
MANTISSA        ({DIGIT}+\.?{DIGIT}*|\.{DIGIT}+)
EXPONENT        ([Ee][+-]?{DIGIT}+)
NUMBER          ({MANTISSA}{EXPONENT}?)
NOTNUMBER       ({MANTISSA}[Ee][+-]?)
IDENT           ({LETTER}({LETTER}|{DIGIT})*)

STRING          (\"([^\\"\n]|\\[\\'"0nt])*\")
CHAR            ('([^\\'\n]|\\[\\'"0nt])')
%%

"#".*           { lexer::include(); }
[ \t]+          { }
\n              { lexer::newline(); }

("if")            { RETURN (TOK_IF); }
("else")          { RETURN (TOK_ELSE); }
("while")         { RETURN (TOK_WHILE); }
("return")        { RETURN (TOK_RET); }
("int")           { RETURN (TOK_INT); }
("string")        { RETURN (TOK_STRING); }
("struct")        { RETURN (TOK_STRUCT); }
("void")          { RETURN (TOK_VOID); }
("new")           { RETURN (TOK_NEW); }
("null")          { RETURN (TOK_NULL); }

(<=)            { RETURN (TOK_LEQ); }
(>=)            { RETURN (TOK_GEQ); }
(==)            { RETURN (TOK_EQ); }
(!=)            { RETURN (TOK_NEQ); }

{STRING}        { RETURN (TOK_STR_CONST); }
{CHAR}          {RETURN (TOK_CHAR_CONST); }
(\[\])          { RETURN (TOK_ARRAY); }
{NUMBER}        { RETURN (TOK_INT_CONST); }
{IDENT}         { RETURN (IDENT); }

"+"             { RETURN ('+'); }
"-"             { RETURN ('-'); }
"*"             { RETURN ('*'); }
"/"             { RETURN ('/'); }

"="             { RETURN ('='); }
"<"             { RETURN ('<'); }
">"             { RETURN ('>'); }

"%"             { RETURN ('%'); }
"!"             { RETURN ('!'); }
"^"             { RETURN ('^'); }

";"             { RETURN (';'); }
"."             { RETURN ('.'); }
","             { RETURN (','); }

"("             { RETURN ('('); }
")"             { RETURN (')'); }
"["             { RETURN ('['); }
"]"             { RETURN (']'); }
"{"             { RETURN ('{'); }
"}"             { RETURN ('}'); }

{NOTNUMBER}     {  lexer::badtoken (yytext);
                  RETURN (NUMBER); }
.               { lexer::badchar (*yytext); }

%%
