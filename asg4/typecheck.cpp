#include <string>
#include <vector>
using namespace std;

#include <stdio.h>
#include <stdlib.h>

#include "lyutils.h"
#include "astree.h"
#include "symtable.h"
#include "typecheck.h"

bool check_void (astree *node) {
   if(!(node->parent->symbol == TOK_FUNCTION
      || node->parent->symbol == TOK_PROTO)) {
      cerr << "Cannot have void in non-function" << endl;
      return false;
   }
   return true;
}

bool check_vardecl (astree* node) {
   astree* n;
   if(node->children[0]->symbol != TOK_ARRAY)
      n = node->children[0]->children[0];
   else
      n = node->children[0]->children[1];

   if(!matches_with(n->attributes,
      node->children[1]->attributes,
      n->struct_name, node->children[1]->struct_name)) {
      cerr << "Incompatible types" << endl;
      return false;
   }
   return true;
}

bool check_return_void (astree *node) {
   node->attributes[ATTR_void] = 1;
   astree* n = node;
   while (n != NULL && n->symbol != TOK_FUNCTION) {
      n = n->parent;
   }
   if (n == NULL) {
      cerr << "Return can only be inside a function" << endl;
      return false;
   }
   if(!n->children[0]->children[0]->attributes[ATTR_void]) {
      cerr << "Incompatible return type" << endl;
      return false;
   }
   return true;
}

bool check_ret (astree* node) {
   astree* n = node;
   while (n != NULL && n->symbol != TOK_FUNCTION) {
      n = n->parent;
   }
   if (n == NULL) {
      cerr << "Cannot have return outside of a function" << endl;
      return false;
   }
   if(!matches_with(n->children[0]->children[0]->attributes,
      node->children[0]->attributes,
      n->children[0]->children[0]->struct_name,
      node->children[0]->struct_name)) {
      cerr << "Incompatible return type" << endl;
      return false;
   }
   return true;
}

bool check_equal_sign (astree* node) {
   if(!matches_with(node->children[0]->attributes,
      node->children[1]->attributes,
      node->children[0]->struct_name,
      node->children[1]->struct_name)
      || !node->children[0]->attributes[ATTR_lval]) {
      cerr << "Incorrect assignment" << endl;
      return false;
   }
   node->attributes[ATTR_string] = node->children[0]->attributes[ATTR_string];
   node->attributes[ATTR_int] = node->children[0]->attributes[ATTR_int];
   node->attributes[ATTR_struct] = node->children[0]->attributes[ATTR_struct];
   node->attributes[ATTR_array] = node->children[0]->attributes[ATTR_array];
   node->struct_name = node->children[0]->struct_name;
   node->attributes[ATTR_vreg] = 1;
   return true;
}

bool check_negative (astree* node) {
   if(!node->children[0]->attributes[ATTR_int]
      && node->children[0]->attributes[ATTR_array]){
      cerr << "Cannot use UNOP on this type" << endl;
      return false;
   }

   node->attributes[ATTR_int] = 1;
   node->attributes[ATTR_vreg] = 1;
   return true;
}

bool check_call (astree* node) {
   vector<astree*> params;
   for(uint i = 1; i < node->children.size(); ++i) {
      params.push_back(node->children[i]);
   }

   string* name = const_cast<string*>(
      node->children[0]->lexinfo);

   symbol* sym;
   if(identifiers.find(name) != identifiers.end()) {
      sym = identifiers.at(name);
   } else {
      cerr << "Undeclared function" << endl;
      return false;
   }

   if(params.size() != sym->parameters->size()) {
      cerr << "Number of parameters given error" << endl;
      return false;
   }
   if(params.size() > 0) {
      for(uint i = 0; i < params.size()-1; ++i) {
         if(!matches_with(params[i]->attributes,
               sym->parameters->at(i)->attributes,
               params[i]->struct_name,
               sym->parameters->at(i)->struct_name)) {
            cerr << "Function parameters mismatch" << endl;
            return false;
         }
      }
   }
   node->attributes = sym->attributes;
   node->attributes[ATTR_vreg] = 1;
   node->attributes[ATTR_function] = 1;
   node->struct_name = sym->struct_name;
   return true;
}

bool check_plus (astree* node) {
   astree* node1 = node->children[0];
   astree* node2 = node->children[1];
   if(!(node1->attributes[ATTR_int]
      && node2->attributes[ATTR_int])
      && (node1->attributes[ATTR_array]
      || node2->attributes[ATTR_array])) {
      cerr<< "BINOP types incompatible" << endl;
      return false;
   }
   node->attributes[ATTR_int] = 1;
   node->attributes[ATTR_vreg] = 1;
   return true;
}

bool check_geq (astree* node) {
   if(!matches_with(node->children[0]->attributes,
      node->children[1]->attributes, 
      node->children[0]->struct_name,
      node->children[1]->struct_name)) {
      cerr << "Unable to compare types" << endl;
      return false;
   }

   node->attributes[ATTR_int] = 1;
   node->attributes[ATTR_vreg] = 1;
   return true;
}

bool check_newstr (astree* node) {
   if(!node->children[1]->attributes[ATTR_int]) {
      cerr << "Size is not integer" << endl;
      return false;
   }
   node->attributes[ATTR_vreg] = 1;
   node->attributes[ATTR_string] = 1;
   return false;
}

bool check_ident (astree* node) {
   string* name = const_cast<string*>(node->lexinfo);
   symbol* sym = get_symbol(node);
   if(sym == NULL) {
      if(identifiers.find(name) != identifiers.end()) {
         sym = identifiers[name];
      } else {
         cerr << "Identifier is undeclared" << endl;
         return false;
      }
   }
   node->attributes = sym->attributes;
   node->struct_name = sym->struct_name;
   return true;
}

bool check_index (astree* node) {
   if(!node->children[1]->attributes[ATTR_int]) {
      cerr << "Integer need to access array";
      return false;
   }

   if(!(node->children[0]->attributes[ATTR_string])
      && !(node->children[0]->attributes[ATTR_array])) {
      cerr << "Improper type for array access" << endl;
      return false;
   }
   if(node->children[0]->attributes[ATTR_string]
      && !node->children[0]->attributes[ATTR_array]) {
      node->attributes[ATTR_int] = 1;
   }
   else {
      node->attributes[ATTR_int] = node->children[0]->attributes[ATTR_int];
      node->attributes[ATTR_string] = node->children[0]->attributes[ATTR_string];
      node->attributes[ATTR_struct] = node->children[0]->attributes[ATTR_struct];
   }

   node->attributes[ATTR_array] = 0;
   node->attributes[ATTR_vaddr] = 1;
   node->attributes[ATTR_lval] = 1;
   node->attributes[ATTR_variable] = 0;
   node->struct_name = node->children[1]->struct_name;
   return true;
}

bool check_period (astree* node) {
   if(!node->children[0]->attributes[ATTR_struct]) {
      cerr << "Incorrect use of Field";
      return false;
   }

   string name = (node->children[0]->struct_name);
   if(fields.find(name) == fields.end()) {
      cerr << "Cannot reference field of undefined struct" << endl;
      return false;
   }
   symbol_table* str = fields.at(name);

   string* field_name = const_cast<string*>(
      node->children[1]->lexinfo);
   
   node->attributes = str->at(field_name)->attributes;
   node->struct_name = str->at(field_name)->struct_name;

   node->attributes[ATTR_field] = 0;
   node->attributes[ATTR_vaddr] = 1;
   node->attributes[ATTR_lval] = 1;
   node->attributes[ATTR_variable] = 1;
   return true;
}

bool check_newarray (astree* node) {
   attr_bitset attrs = node->children[0]->attributes;
   if(attrs[ATTR_void]
      || attrs[ATTR_array]
      || attrs[ATTR_void]
      || (attrs[ATTR_struct]
         && node->children[0]->struct_name == "")) {
      cerr << "Invalid array type" << endl;
      return false;
   }
   if(attrs[ATTR_int])
      node->attributes[ATTR_int] = 1;
   else if(attrs[ATTR_string])
      node->attributes[ATTR_string] = 1;

   node->attributes[ATTR_vreg] = 1;
   node->attributes[ATTR_array] = 1;
   return true;
}

bool check_type (astree* node) {
   switch (node->symbol) {
      case TOK_TYPEID: {
            string* name = const_cast<string*>(node->lexinfo);
            if(structs.find(name) == structs.end()) {
               break;
            }
            symbol* str = structs.at(name);
            node->attributes[ATTR_struct] = 1;
            node->struct_name = str->struct_name;
         break;
      }
      case TOK_VOID: {
         check_void (node);
         break;
      }
      case TOK_VARDECL: {
          check_vardecl (node);
          break;
      }
      case TOK_RETURNVOID: {
         check_return_void (node);
         break;
      }
      case TOK_RET: {
         check_ret (node);
         break;
      }
      case '=': {
         check_equal_sign (node);
         break;
      }
      case TOK_EXC:
      case TOK_POS:
      case TOK_NEG: {
         check_negative (node);
         break;
      }
      case TOK_NEW: {
         node->attributes = node->children[0]->attributes;
         node->struct_name = node->children[0]->struct_name;
         node->attributes[ATTR_vreg] = 1;
         break;
      }
      case TOK_CALL: {
         check_call (node);
         break;
      }
      case '-':
      case '*':
      case '/':
      case '%':
      case '+': {
         check_plus (node);
         break;
      }
      case TOK_EQ:
      case TOK_NEQ:
      case '<':
      case TOK_LEQ:
      case '>':
      case TOK_GEQ: {
         check_geq (node);
         break;
      }
      case TOK_NEWSTR: {
         check_newstr (node);
         break;
      }
      case TOK_IDENT: {
         check_ident (node);
         break;
      }
      case TOK_INDEX: {
         check_index (node);
         break;
      }
      case '.': {
         // ADD CHECKS FOR STRUCT COMPLETENESS
         check_period (node);
         break;
      }
      case TOK_NULL: {
         node->attributes[ATTR_null] = 1;
         node->attributes[ATTR_const] = 1;
         break;
      }
      case TOK_STR_CONST: {
         node->attributes[ATTR_const] = 1;
         node->attributes[ATTR_string] = 1;
         break;
      }
      case TOK_INT_CONST: {
         node->attributes[ATTR_const] = 1;
         node->attributes[ATTR_int] = 1;
         break;
      }
      case TOK_CHAR_CONST: {
         node->attributes[ATTR_const] = 1;
         node->attributes[ATTR_int] = 1;
         break;
      }
      case TOK_INT: {
         node->attributes[ATTR_int] = 1;
         break;
      }
      case TOK_STRING: {
         node->attributes[ATTR_string] = 1;
         break;
      }
      case TOK_NEWARRAY: {
         check_newarray (node);
         break;
      }
      default: break;
   }
   return true;
}
