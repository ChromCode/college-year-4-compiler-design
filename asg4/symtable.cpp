#include "symtable.h"

symbol_table structs;
symbol_table identifiers;

unordered_map<string, symbol_table*> fields;

int blk_num;
int next_blk;

vector<int> blk_stack;
vector<symbol_table*> symbol_stack;

symbol* get_symbol (astree* node) {
   string* name = const_cast<string*>(node->lexinfo);
   for(int i = symbol_stack.size()-1; i >= 0; --i) {
      if(symbol_stack[i] == nullptr) {
         continue;
      }
      if(symbol_stack[i]->find(name) != symbol_stack[i]->end()) {
         if(symbol_stack[i]->at(name)->attributes[ATTR_function] == 0){
            return symbol_stack[i]->at(name);
         }
      }
   }
   return NULL;
}

symbol* setup_function (astree* node) {
   symbol* sym = new symbol;
   if(node->children[0]->symbol != TOK_ARRAY) {
      set_attributes(node->children[0], sym);
   } else {
      set_attributes(node->children[0]->children[0], sym);
   }
   sym->attributes[ATTR_function] = 1;

   return sym;
}

symbol* set_symbol (astree* node) {
   symbol* sym = new symbol;
   set_attributes(node, sym);
   sym->attributes[ATTR_lval] = 1;
   sym->attributes[ATTR_variable] = 1;
   return sym;
}

void print_attributes (symbol* sym, string* name, FILE* out) {
   attr_bitset attr = sym->attributes;
   string list;

   for(uint i = 0; i < blk_stack.size()-1; ++i) {
      fprintf(out, "\t");
   }
   if (attr[ATTR_field] == 0) list += "{"+to_string(sym->block_nr)+"} ";

   if (attr[ATTR_const] == 1) {
      list += "const ";
   } if (attr[ATTR_field] == 1) {
      list += "field {" + sym->struct_name + "} ";
   } if (attr[ATTR_function] == 1) {
      list += "function ";
   } if (attr[ATTR_int] == 1) {
      list += "int ";
   } if (attr[ATTR_lval] == 1) {
      list += "lval ";
   } if (attr[ATTR_null] == 1) {
      list += "null ";
   } if (attr[ATTR_param] == 1) {
      list += "param ";
   } if (attr[ATTR_string] == 1) {
      list += "string ";
   } if (attr[ATTR_struct] == 1) {
      list += "struct \"" + sym->struct_name + "\" ";
   } if (attr[ATTR_vaddr] == 1) {
      list += "vaddr ";
   } if (attr[ATTR_variable] == 1) {
      list += "variable ";
   } if (attr[ATTR_void] == 1) {
      list += "void ";
   } if (attr[ATTR_vreg] == 1) {
      list += "vreg ";
   }

   fprintf(out, "%s (%zu.%zu.%zu) %s\n", 
      (*name).c_str(), sym->filenr, sym->linenr, 
      sym->offset, list.c_str());
}

void set_attributes (astree* node, symbol* sym) {
   sym->filenr = node->lloc.filenr;
   sym->linenr = node->lloc.linenr;
   sym->offset = node->lloc.offset;
   sym->block_nr = blk_stack.back();
   switch (node->symbol) {
      case TOK_INT: {
         sym->attributes[ATTR_int] = 1;
         break;
      }
      case TOK_NULL: {
         sym->attributes[ATTR_null] = 1;
         break;
      }
      case TOK_STRING: {
         sym->attributes[ATTR_string] = 1;
         break;
      }
      case TOK_TYPEID: {
         sym->attributes[ATTR_struct] = 1;
         sym->struct_name = string(*(node->lexinfo));
         break;
      }
      case TOK_VOID: {
         sym->attributes[ATTR_void] = 1;
         break;
      }
   }
}


bool var_search (string* name) {
   bool found = false;
   for(int i = symbol_stack.size()-1; i >= 0; --i) {
      if(symbol_stack[i] == nullptr) {
         continue;
      }
     if(symbol_stack[i]->find(name) != symbol_stack[i]->end()) {
         if(symbol_stack[i]->at(name)->attributes[ATTR_function] == 0){
            found = true;
            break;
         }
      }
   }
   if(identifiers.find(name) != identifiers.end()) found = true;
   return found;
}

bool in_scope_var_search (string* name) {
   bool found = false;
   symbol_table* back = symbol_stack.back();
   if(back != nullptr) {
      if(back->find(name) != back->end()) {
         if(!back->at(name)->attributes[ATTR_function]){
            found = true;
         }
      }
   }
   return found;
}

bool check_return (symbol* sym1, symbol* sym2) {
   attr_bitset bit1 = sym1->attributes;
   attr_bitset bit2 = sym2->attributes;
   
   if((bit1[ATTR_int] && bit2[ATTR_int])
      || (bit1[ATTR_void] && bit2[ATTR_void])
      || (bit1[ATTR_string] && bit2[ATTR_string]))
         return false;
   return true; 
}

bool matches_with(attr_bitset attr1, attr_bitset attr2, 
                              string struct1, string struct2) {
   if((attr1[ATTR_null] && attr2[ATTR_struct])
   || (attr1[ATTR_struct] && attr2[ATTR_null])
   || (attr1[ATTR_null] && attr2[ATTR_string])
   || (attr1[ATTR_string] && attr2[ATTR_null])
   || (attr1[ATTR_null] && attr2[ATTR_array])
   || (attr1[ATTR_array] && attr2[ATTR_null]))
     return true;

   if ((attr1[ATTR_int] != attr2[ATTR_int])
   || (attr1[ATTR_string] != attr2[ATTR_string])
   || (attr1[ATTR_array] != attr2[ATTR_array])
   || (attr1[ATTR_struct] != attr2[ATTR_struct])
   || (attr1[ATTR_void] != attr2[ATTR_void])
   || ((attr1[ATTR_struct] != attr2[ATTR_struct]) 
      &&(struct1 != struct2)))
      return false;

   return true;
}

bool check_void (astree *node) {
   if(!(node->parent->symbol == TOK_FUNCTION
      || node->parent->symbol == TOK_PROTO)) {
      cerr << "Cannot have void in non-function" << endl;
      return false;
   }
   return true;
}

bool check_vardecl (astree* node) {
   astree* n;
   if(node->children[0]->symbol != TOK_ARRAY)
      n = node->children[0]->children[0];
   else
      n = node->children[0]->children[1];

   if(!matches_with(n->attributes,
      node->children[1]->attributes,
      n->struct_name, node->children[1]->struct_name)) {
      cerr << "Incompatible types" << endl;
      return false;
   }
   return true;
}

bool check_return_void (astree *node) {
   node->attributes[ATTR_void] = 1;
   astree* n = node;
   while (n != NULL && n->symbol != TOK_FUNCTION) {
      n = n->parent;
   }
   if (n == NULL) {
      cerr << "Return can only be inside a function" << endl;
      return false;
   }
   if(!n->children[0]->children[0]->attributes[ATTR_void]) {
      cerr << "Incompatible return type" << endl;
      return false;
   }
   return true;
}

bool check_ret (astree* node) {
   astree* n = node;
   while (n != NULL && n->symbol != TOK_FUNCTION) {
      n = n->parent;
   }
   if (n == NULL) {
      cerr << "Cannot have return outside of a function" << endl;
      return false;
   }
   if(!matches_with(n->children[0]->children[0]->attributes,
      node->children[0]->attributes,
      n->children[0]->children[0]->struct_name,
      node->children[0]->struct_name)) {
      cerr << "Incompatible return type" << endl;
      return false;
   }
   return true;
}

bool check_equal_sign (astree* node) {
   if(!matches_with(node->children[0]->attributes,
      node->children[1]->attributes,
      node->children[0]->struct_name,
      node->children[1]->struct_name)
      || !node->children[0]->attributes[ATTR_lval]) {
      cerr << "Incorrect assignment" << endl;
      return false;
   }
   node->attributes[ATTR_string] = node->children[0]->attributes[ATTR_string];
   node->attributes[ATTR_int] = node->children[0]->attributes[ATTR_int];
   node->attributes[ATTR_struct] = node->children[0]->attributes[ATTR_struct];
   node->attributes[ATTR_array] = node->children[0]->attributes[ATTR_array];
   node->struct_name = node->children[0]->struct_name;
   node->attributes[ATTR_vreg] = 1;
   return true;
}

bool check_negative (astree* node) {
   if(!node->children[0]->attributes[ATTR_int]
      && node->children[0]->attributes[ATTR_array]){
      cerr << "Cannot use UNOP on this type" << endl;
      return false;
   }

   node->attributes[ATTR_int] = 1;
   node->attributes[ATTR_vreg] = 1;
   return true;
}

bool check_call (astree* node) {
   vector<astree*> params;
   for(uint i = 1; i < node->children.size(); ++i) {
      params.push_back(node->children[i]);
   }

   string* name = const_cast<string*>(
      node->children[0]->lexinfo);

   symbol* sym;
   if(identifiers.find(name) != identifiers.end()) {
      sym = identifiers.at(name);
   } else {
      cerr << "Undeclared function" << endl;
      return false;
   }

   if(params.size() != sym->parameters->size()) {
      cerr << "Number of parameters given error" << endl;
      return false;
   }
   if(params.size() > 0) {
      for(uint i = 0; i < params.size()-1; ++i) {
         if(!matches_with(params[i]->attributes,
               sym->parameters->at(i)->attributes,
               params[i]->struct_name,
               sym->parameters->at(i)->struct_name)) {
            cerr << "Function parameters mismatch" << endl;
            return false;
         }
      }
   }
   node->attributes = sym->attributes;
   node->attributes[ATTR_vreg] = 1;
   node->attributes[ATTR_function] = 1;
   node->struct_name = sym->struct_name;
   return true;
}

bool check_plus (astree* node) {
   astree* node1 = node->children[0];
   astree* node2 = node->children[1];
   if(!(node1->attributes[ATTR_int]
      && node2->attributes[ATTR_int])
      && (node1->attributes[ATTR_array]
      || node2->attributes[ATTR_array])) {
      cerr<< "BINOP types incompatible" << endl;
      return false;
   }
   node->attributes[ATTR_int] = 1;
   node->attributes[ATTR_vreg] = 1;
   return true;
}

bool check_geq (astree* node) {
   if(!matches_with(node->children[0]->attributes,
      node->children[1]->attributes, 
      node->children[0]->struct_name,
      node->children[1]->struct_name)) {
      cerr << "Unable to compare types" << endl;
      return false;
   }

   node->attributes[ATTR_int] = 1;
   node->attributes[ATTR_vreg] = 1;
   return true;
}

bool check_newstr (astree* node) {
   if(!node->children[1]->attributes[ATTR_int]) {
      cerr << "Size is not integer" << endl;
      return false;
   }
   node->attributes[ATTR_vreg] = 1;
   node->attributes[ATTR_string] = 1;
   return false;
}

bool check_ident (astree* node) {
   string* name = const_cast<string*>(node->lexinfo);
   symbol* sym = get_symbol(node);
   if(sym == NULL) {
      if(identifiers.find(name) != identifiers.end()) {
         sym = identifiers[name];
      } else {
         cerr << "Identifier is undeclared" << endl;
         return false;
      }
   }
   node->attributes = sym->attributes;
   node->struct_name = sym->struct_name;
   return true;
}

bool check_index (astree* node) {
   if(!node->children[1]->attributes[ATTR_int]) {
      cerr << "Integer need to access array";
      return false;
   }

   if(!(node->children[0]->attributes[ATTR_string])
      && !(node->children[0]->attributes[ATTR_array])) {
      cerr << "Improper type for array access" << endl;
      return false;
   }
   if(node->children[0]->attributes[ATTR_string]
      && !node->children[0]->attributes[ATTR_array]) {
      node->attributes[ATTR_int] = 1;
   }
   else {
      node->attributes[ATTR_int] = node->children[0]->attributes[ATTR_int];
      node->attributes[ATTR_string] = node->children[0]->attributes[ATTR_string];
      node->attributes[ATTR_struct] = node->children[0]->attributes[ATTR_struct];
   }

   node->attributes[ATTR_array] = 0;
   node->attributes[ATTR_vaddr] = 1;
   node->attributes[ATTR_lval] = 1;
   node->attributes[ATTR_variable] = 0;
   node->struct_name = node->children[1]->struct_name;
   return true;
}

bool check_period (astree* node) {
   if(!node->children[0]->attributes[ATTR_struct]) {
      cerr << "Incorrect use of Field";
      return false;
   }

   string name = (node->children[0]->struct_name);
   if(fields.find(name) == fields.end()) {
      cerr << "Cannot reference field of undefined struct" << endl;
      return false;
   }
   symbol_table* str = fields.at(name);

   string* field_name = const_cast<string*>(
      node->children[1]->lexinfo);
   
   node->attributes = str->at(field_name)->attributes;
   node->struct_name = str->at(field_name)->struct_name;

   node->attributes[ATTR_field] = 0;
   node->attributes[ATTR_vaddr] = 1;
   node->attributes[ATTR_lval] = 1;
   node->attributes[ATTR_variable] = 1;
   return true;
}

bool check_newarray (astree* node) {
   attr_bitset attrs = node->children[0]->attributes;
   if(attrs[ATTR_void]
      || attrs[ATTR_array]
      || attrs[ATTR_void]
      || (attrs[ATTR_struct]
         && node->children[0]->struct_name == "")) {
      cerr << "Invalid array type" << endl;
      return false;
   }
   if(attrs[ATTR_int])
      node->attributes[ATTR_int] = 1;
   else if(attrs[ATTR_string])
      node->attributes[ATTR_string] = 1;

   node->attributes[ATTR_vreg] = 1;
   node->attributes[ATTR_array] = 1;
   return true;
}

bool check_type (astree* node) {
   switch (node->symbol) {
      case TOK_TYPEID: {
            string* name = const_cast<string*>(node->lexinfo);
            if(structs.find(name) == structs.end()) {
               break;
            }
            symbol* str = structs.at(name);
            node->attributes[ATTR_struct] = 1;
            node->struct_name = str->struct_name;
         break;
      }
      case TOK_VOID: {
         check_void (node);
         break;
      }
      case TOK_VARDECL: {
          check_vardecl (node);
          break;
      }
      case TOK_RETURNVOID: {
         check_return_void (node);
         break;
      }
      case TOK_RET: {
         check_ret (node);
         break;
      }
      case '=': {
         check_equal_sign (node);
         break;
      }
      case TOK_EXC:
      case TOK_POS:
      case TOK_NEG: {
         check_negative (node);
         break;
      }
      case TOK_NEW: {
         node->attributes = node->children[0]->attributes;
         node->struct_name = node->children[0]->struct_name;
         node->attributes[ATTR_vreg] = 1;
         break;
      }
      case TOK_CALL: {
         check_call (node);
         break;
      }
      case '-':
      case '*':
      case '/':
      case '%':
      case '+': {
         check_plus (node);
         break;
      }
      case TOK_EQ:
      case TOK_NEQ:
      case '<':
      case TOK_LEQ:
      case '>':
      case TOK_GEQ: {
         check_geq (node);
         break;
      }
      case TOK_NEWSTR: {
         check_newstr (node);
         break;
      }
      case TOK_IDENT: {
         check_ident (node);
         break;
      }
      case TOK_INDEX: {
         check_index (node);
         break;
      }
      case '.': {
         // ADD CHECKS FOR STRUCT COMPLETENESS
         check_period (node);
         break;
      }
      case TOK_NULL: {
         node->attributes[ATTR_null] = 1;
         node->attributes[ATTR_const] = 1;
         break;
      }
      case TOK_STR_CONST: {
         node->attributes[ATTR_const] = 1;
         node->attributes[ATTR_string] = 1;
         break;
      }
      case TOK_INT_CONST: {
         node->attributes[ATTR_const] = 1;
         node->attributes[ATTR_int] = 1;
         break;
      }
      case TOK_CHAR_CONST: {
         node->attributes[ATTR_const] = 1;
         node->attributes[ATTR_int] = 1;
         break;
      }
      case TOK_INT: {
         node->attributes[ATTR_int] = 1;
         break;
      }
      case TOK_STRING: {
         node->attributes[ATTR_string] = 1;
         break;
      }
      case TOK_NEWARRAY: {
         check_newarray (node);
         break;
      }
      default: break;
   }
   return true;
}

bool scan_table (astree* node, FILE* out) {
   node->block_nr = blk_stack.back();   
   switch (node->symbol) {
      case TOK_BLOCK: {
         blk_stack.push_back(next_blk++);
         symbol_stack.push_back(nullptr);
         break;
      }
      case TOK_FUNCTION: {
         string* ident = const_cast<string*>(
            node->children[0]->children[0]->lexinfo);

         if(node->children[0]->symbol == TOK_ARRAY) {
            ident = const_cast<string*>(
               node->children[0]->children[1]->lexinfo);
         }
        
         if(identifiers.find(ident) != identifiers.end()) {
            if(identifiers[ident]->attributes[ATTR_function] == 1) {
               cerr << "Function already declared" << endl;
               break;
            }
         }
         if(symbol_stack.back() == nullptr) {
            symbol_stack.pop_back();
            symbol_stack.push_back(new symbol_table);
         }

         symbol* sym = setup_function(node);
         astree* tree0 = node->children[0];
         print_attributes(sym, ident, out);

         if(tree0->symbol != TOK_ARRAY) {
            tree0->children[0]->attributes = sym->attributes;
            tree0->children[0]->struct_name = sym->struct_name;
         } else {
            tree0->children[1]->attributes = sym->attributes;
            tree0->children[1]->struct_name = sym->struct_name;
         }

         sym->block_nr = GLOBAL;
         blk_stack.push_back(next_blk++);
         sym->parameters = new vector<symbol*>;
         symbol_stack.push_back(new symbol_table);

         for(uint i = 0; i < node->children[1]->children.size(); ++i) {
            astree* current = node->children[1]->children[i];
            symbol* param;
            if(current->symbol == TOK_ARRAY)
               param = set_symbol(current->children[0]);
            else
               param = set_symbol(current);

            param->attributes[ATTR_param] = 1;
            sym->parameters->push_back(param);

            if(current->symbol != TOK_ARRAY) {
               current->children[0]->attributes = param->attributes;
               current->children[0]->block_nr = param->block_nr;
               current->children[0]->struct_name = param->struct_name;
            } else {
               param->attributes[ATTR_array] = 1;
               current->children[1]->attributes = param->attributes;
               current->children[1]->block_nr = param->block_nr;
               current->children[1]->struct_name = param->struct_name;
            }

            string* param_ident;

            if(current->symbol == TOK_ARRAY)
               param_ident = const_cast<string*>(
               current->children[1]->lexinfo);
            else
               param_ident = const_cast<string*>(
               current->children[0]->lexinfo);

            symbol_entry param_entry (param_ident, param);
            symbol_stack.back()->insert(param_entry);
            print_attributes (param, param_ident, out);
         }

         if(identifiers.find(ident) != identifiers.end()) {
            if(identifiers[ident]->attributes[ATTR_prototype] == 1) {
               if(identifiers[ident]->parameters != sym->parameters) {
                  cerr << "Mismatched parameters " << endl;
               }
               if(check_return(identifiers[ident], sym) == false) {
                  cerr << "Mismatched return types" << endl;
               }
            }
         }

         symbol_entry entry (ident, sym);
         symbol_stack.back()->insert(entry);
         identifiers.insert(entry);
         fprintf(out, "\n");

         break;
      }
      case TOK_PROTO: {
         astree* t0 = node->children[0];
         string* ident = const_cast<string*>(t0->children[0]->lexinfo);
         if(t0->symbol == TOK_ARRAY)
            ident = const_cast<string*>(t0->children[1]->lexinfo);

         if(identifiers.find(ident) != identifiers.end()) {
            if(identifiers[ident]->attributes[ATTR_function] == 1) {
               cerr << "Redclaration of Prototype" << endl;
               break;
            }
         }

         if(symbol_stack.back() == nullptr) {
            symbol_stack.pop_back();
            symbol_stack.push_back(new symbol_table);
         }

         symbol* sym = setup_function(node);
         sym->attributes[ATTR_prototype] = 1;
         blk_stack.push_back(next_blk++);
         sym->parameters = new vector<symbol*>;

         if(t0->symbol == TOK_ARRAY) {
            sym->attributes[ATTR_array] = 1;
            t0->children[1]->attributes = sym->attributes;
            t0->children[0]->attributes[ATTR_array] = 1;
         }
         else {
            t0->children[0]->attributes = sym->attributes;
         }

         symbol_stack.push_back(new symbol_table);
         astree* t1 = node->children[1];
         for(uint i = 0; i < t1->children.size(); ++i) {
            astree* current = node->children[1]->children[i];
            symbol* param = set_symbol(t1->children[i]);
            param->attributes[ATTR_param] = 1;
            sym->parameters->push_back(param);
            current->children[0]->attributes = param->attributes;
            current->children[0]->block_nr = param->block_nr;
            current->children[0]->struct_name = param->struct_name;

            string* param_name = const_cast<string*>(
               current->children[0]->lexinfo);
            symbol_entry param_entry (param_name, param);
            symbol_stack.back()->insert(param_entry);
            print_attributes (param, param_name, out);
         }
         symbol_entry entry (ident, sym);
         symbol_stack.back()->insert(entry);
         identifiers.insert(entry);

         break;
      }
      case TOK_STRUCT: {

         string* ident =const_cast<string*>(node->children[0]->lexinfo);
         symbol* sym = new symbol;
         sym->attributes[ATTR_struct] = 1;
         sym->struct_name = *ident;
         set_attributes(node, sym);
         sym->block_nr = GLOBAL;
         node->attributes = sym->attributes;
         node->struct_name = sym->struct_name;
         symbol_table* tbl = new symbol_table;
         bool valid = true;
         symbol_entry entry (ident, sym);
         structs.insert(entry);
         print_attributes(sym, ident, out);
        
         if(node->children.size() > 1) {
            for(uint i = 0; 
               i < node->children[1]->children.size(); 
               ++i) {
            
               astree* current = node->children[1]->children[i];
               if(current->symbol == TOK_TYPEID) {
                  if(structs.find(ident) == structs.end()) {
                     cerr << "No instance of struct found" << endl;
                     valid = false;
                     break;
                  }
               }
               if(valid == false) break;
               fprintf(out, "\t");
               string* field_ident = const_cast<string*>(
                  current->children[0]->lexinfo);

               if(current->symbol == TOK_ARRAY)
                  field_ident = const_cast<string*>(
                     current->children[1]->lexinfo);

               symbol* sym = new symbol;
               if(current->symbol == TOK_ARRAY) {
                  set_attributes(current->children[0], sym);
                  sym->attributes[ATTR_array] = 1;
               }
               else
                  set_attributes(current, sym);

               sym->attributes[ATTR_field] = 1;
               sym->struct_name = *(const_cast<string*>(
                  current->lexinfo));
               sym->attributes[ATTR_field] = 1;
               sym->block_nr = GLOBAL;
               symbol_entry field (field_ident, sym);
               tbl->insert(field);

               print_attributes(sym, field_ident, out);
               current->children[0]->attributes = sym->attributes;
               current->children[0]->struct_name = *ident;
            }
         }
         pair <string, symbol_table*> str (*ident, tbl);
         fields.insert(str);
         sym->fields = tbl;
         fprintf(out, "\n");
         break;
      }
      case TOK_VARDECL: {
         if(symbol_stack.back() == nullptr) {
            symbol_stack.back() = new symbol_table;
         }
         string* ident = const_cast<string*>(
            node->children[0]->children[0]->lexinfo);

         if(node->children[0]->symbol == TOK_ARRAY)
            ident = const_cast<string*>(
               node->children[0]->children[1]->lexinfo);
         if(in_scope_var_search(ident)) {
            cerr << "Variable redeclared" << endl;
            return false;
         }

         symbol* sym = new symbol;
         if(node->children[1]->symbol == TOK_IDENT) {
            string* find_ident = const_cast<string*>(
               node->children[1]->lexinfo);

            if(!var_search(find_ident)) {
               cerr << "Unable to assign value to variable" << endl;
               break;
            }
            symbol* assignee = get_symbol(node->children[1]);
            set_attributes(node->children[0], sym);
            sym->attributes = assignee->attributes;
            sym->struct_name = assignee->struct_name;
            sym->parameters = assignee->parameters;
            sym->fields = assignee->fields;
         }
         else {
            if(node->children[0]->symbol == TOK_ARRAY) {
               sym = set_symbol(node->children[0]->children[0]);
               sym->attributes[ATTR_array] = 1;
            }
            else
               sym = set_symbol(node->children[0]);
         }

         symbol_entry entry (ident, sym);
         symbol_stack.back()->insert(entry);
         astree* t0 = node->children[0];
         if(t0->symbol == TOK_ARRAY) {
            t0->children[1]->attributes = sym->attributes;
            t0->children[1]->struct_name = sym->struct_name;
            t0->children[0]->attributes[ATTR_array] = 1;
         } else {
            t0->children[0]->attributes = sym->attributes;
            t0->children[0]->struct_name = sym->struct_name;
         }
         print_attributes(sym, ident, out);
         break;
      }
      default: break;
   }

   for (astree* child: node->children) {
      bool ret = scan_table(child, out);
      if(ret == false) return false;
   }

   bool good = check_type(node);

   if(good == false) return false;

   switch (node->symbol) {
      case TOK_IDENT: {
         string* ident = const_cast<string*>(node->lexinfo);
         if(var_search(ident) == false) {
            cerr << "Undeclared Variable" << endl;
         }
         break;
      }
      default: break;
   }
   
   if(node->symbol == TOK_BLOCK
      || node->symbol == TOK_FUNCTION
      || node->symbol == TOK_PROTO) {
      symbol_stack.pop_back();
      blk_stack.pop_back();
   }

   return true;
}

void setup_symtable() {
   symbol_stack.push_back(new symbol_table);
   blk_stack.push_back(next_blk++);
}

