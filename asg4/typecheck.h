#ifndef __TYPECHECK_H__
#define __TYPECHECK_H__

#include <string>
#include <bitset>
#include <unordered_map>
#include <utility>
using namespace std;

void type_check (astree *node);

#endif